export enum Priority {
  HIGH = 'HIGH',
  MEDIUM = 'MEDIUM',
  LOW = 'LOW',
}

export interface Todo {
  id: number;
  task: string;
  priority: Priority;
  completed: boolean;
  createdAt: Date;
  completedAt: Date;
  deletedAt: Date;
}
