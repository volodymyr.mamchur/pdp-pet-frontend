import NextAuth from 'next-auth';
import GoogleProvider from 'next-auth/providers/google';

export default NextAuth({
  providers: [
    GoogleProvider({
      clientId: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    }),
  ],
  secret: 'some_secret',
  callbacks: {
    session: async ({session, token, user}) => {
      // todo: do something with session, jwt token or user. mostly uses for the server-side auth functionality.
      return session;
    },
  },
});
