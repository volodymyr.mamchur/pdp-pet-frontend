import Layout from '../components/layout';

export default function Home() {
  return (
    <Layout>
      <h1>PDP PET</h1>
      <p>
        This is an example site to demonstrate how to use next, next-auth, swr.
      </p>
    </Layout>
  )
}
