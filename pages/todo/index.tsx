import { GetStaticProps, InferGetStaticPropsType } from 'next';
import Link from 'next/link';
import _ from 'lodash';
import axios from 'axios';

import { Todo } from '../../types/todo';
import Layout from '../../components/layout';

import styles from './index.module.css';


interface Props {
  todos: Array<Todo>,
}

export default function Todos({ todos }: Props) {
// export default function Home({ todos }: InferGetStaticPropsType<typeof getStaticProps>) {
  const renderTodos = () => {
    return _.map(todos, ({ id, task, priority, completed, createdAt }: Todo) => (
      <div className={styles.todo} key={id}>
        <Link href={`/todo/${id}`}>
          <a><h3>Task: {task}</h3></a>
        </Link>
        <p>Priority: {priority}</p>
        <p>Completed: {completed ? 'true' : 'false'}</p>
        <p>Created at: {(new Date(createdAt)).toString()}</p>
      </div>
    ));
  };

  return (
    <Layout>
      <h1>Todos:</h1>
      {renderTodos()}
    </Layout>
  );
}

export const getStaticProps: GetStaticProps<Props> = async () => {
  const { data } = await axios.get('http://localhost:8000/api/todo');

  return {
    props: {
      todos: data,
    },
    revalidate: 10,
  };
};
