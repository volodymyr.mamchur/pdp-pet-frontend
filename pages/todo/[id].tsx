import { GetStaticProps, GetStaticPaths, InferGetStaticPropsType } from 'next';
import Link from 'next/link';
import _ from 'lodash';
import axios from 'axios';

import { Todo } from '../../types/todo';
import Layout from '../../components/layout';

import styles from './index.module.css';

export const getStaticPaths: GetStaticPaths = async () => {
  const { data } = await axios.get<Array<Todo>>('http://localhost:8000/api/todo');
  const paths = data.map(({ id }) => ({ params: { id: id.toString() } }));

  return { paths, fallback: false };
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  const { data } = await axios.get<Todo>(`http://localhost:8000/api/todo/${params.id}`);

  return {
    props: {
      todo: data,
    },
  };
};


export default function TodoItem({ todo }: InferGetStaticPropsType<typeof getStaticProps>) {
  return (
    <Layout>
      <h1>Todo #{todo.id}</h1>
      <div className={styles.todo} key={todo.id}>
        <h3>Task: {todo.task}</h3>
        <p>Priority: {todo.priority}</p>
        <p>Completed: {todo.completed ? 'true' : 'false'}</p>
        <p>Created at: {(new Date(todo.createdAt)).toString()}</p>
      </div>
    </Layout>
  );
}


