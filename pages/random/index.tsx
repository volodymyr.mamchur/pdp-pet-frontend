import {GetServerSideProps, InferGetStaticPropsType } from 'next';
import axios from 'axios';
import _ from 'lodash';

import Layout from '../../components/layout';

import styles from './index.module.css';
import Link from 'next/link';

interface Props {
  random: Array<number>
}

export const getServerSideProps: GetServerSideProps<Props> = async () => {
  const { data } = await axios.get<Array<number>>(`http://localhost:8000/api/random`);

  return {
    props: {
      random: data,
    },
  };
};


export default function Todo({ random }: Props) {
  const renderRandom = () => {
    return _.map(random, (number) => (
      <li>{number}</li>
    ));
  };

  return (
    <Layout>
      <h1>Random numbers</h1>
      <ul>{renderRandom()}</ul>
    </Layout>
  );
}


