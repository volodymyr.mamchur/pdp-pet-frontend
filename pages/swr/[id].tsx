import { GetStaticPaths, GetStaticProps } from 'next';
import axios from 'axios';

import { Todo } from '../../types/todo';
import Layout from '../../components/layout';

import styles from './index.module.css';
import useSwr, { mutate, useSWRConfig } from 'swr';

function fetcher(url: string) {
  return axios.get<Todo>(url).then(res => res.data);
}

export const getStaticPaths: GetStaticPaths = async () => {
  const { data } = await axios.get<Array<Todo>>('http://localhost:8000/api/todo');
  const paths = data.map(({ id }) => ({ params: { id: id.toString() } }));

  return { paths, fallback: false };
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  return {
    props: {
      id: params.id,
    },
  };
};

export default function TodoItem({ id }: {id: string}) {
  const { mutate } = useSWRConfig();
  const { error, data: todo } = useSwr(`http://localhost:8000/api/todo/${id}`, fetcher);

  const handleChangeCheckBoxValue = async () => {
    await mutate(`http://localhost:8000/api/todo/${id}`, { ...todo, completed: !todo.completed }, false);
    await axios.patch(`http://localhost:8000/api/todo/${id}`, {
      completed: !todo.completed,
    });
  };

  const renderPlaceholder = () => {
    return (<div>Loading...</div>);
  };

  const renderError = () => {
    return (<div>Some error occurred</div>);
  };

  const renderContent = () => {
    return (
      <>
        <h1>Todo #{todo.id}</h1>
        <div className={styles.todo} key={todo.id}>
          <h3>Task: {todo.task}</h3>
          <p>Priority: {todo.priority}</p>
          <p>Completed: <input type="checkbox" checked={todo.completed} onChange={handleChangeCheckBoxValue}/> (click
            here)</p>
          <p>Created at: {(new Date(todo.createdAt)).toString()}</p>
        </div>
      </>
    );
  };

  return (
    <Layout>
      {error && renderError()}
      {!error && !todo && renderPlaceholder()}
      {!error && !!todo && renderContent()}
    </Layout>
  );
}


