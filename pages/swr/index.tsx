import Link from 'next/link';
import _ from 'lodash';
import useSwr from 'swr';

import { Todo } from '../../types/todo';
import Layout from '../../components/layout';

import styles from './index.module.css';
import axios from 'axios';

function fetcher(url: string) {
  return axios.get<Array<Todo>>(url).then(res => res.data);
}

export default function TodosSwr() {
  const { data, error } = useSwr('http://localhost:8000/api/todo', fetcher, {
    refreshInterval: 10000,
  });

  const renderPlaceholder = () => {
    return (<div>Loading...</div>);
  };

  const renderError = () => {
    return (<div>Some error occurred</div>);
  };

  const renderTodos = () => {
    return _.map(data, ({ id, task, priority, completed, createdAt }: Todo) => (
      <div className={styles.todo} key={id}>
        <Link href={`/swr/${id}`}>
          <a><h3>Task: {task}</h3></a>
        </Link>
        <p>Priority: {priority}</p>
        <p>Completed: {completed ? 'true' : 'false'}</p>
        <p>Created at: {(new Date(createdAt)).toString()}</p>
      </div>
    ));
  };

  return (
    <Layout>
      <h1>Todos:</h1>
      {error && renderError()}
      {!error && !data && renderPlaceholder()}
      {!error && !!data && renderTodos()}
    </Layout>
  );
}
