import Link from 'next/link';
import { signIn, signOut, useSession } from 'next-auth/react';

import styles from './header.module.css';

export default function Header() {
  const { data: session, status } = useSession();
  const loading = status === 'loading';

  const handleSingIn = () => {
    signIn();
  };

  const handleSingOut = () => {
    signOut();
  };

  const renderSignedInContainer = () => {
    return (
      <>
        {session.user.image && (
          <span
            style={{ backgroundImage: `url('${session.user.image}')` }}
            className={styles.avatar}
          />
        )}

        <span className={styles.signedInText}>
          <small>Signed in as</small>
          <br/>
          <strong>{session.user.email || session.user.name}</strong>
        </span>

        <button className={styles.button} onClick={handleSingOut}>Sign out</button>
      </>
    );
  };

  const renderSignInContainer = () => {
    return (
      <>
        <span className={styles.notSignedInText}>You are not signed in</span>
        <button className={styles.button} onClick={handleSingIn}>Sign in</button>
      </>
    );
  };

  return (
    <header className={styles.header}>
      <div className={styles.signInContainer}>
        {session ? renderSignedInContainer() : renderSignInContainer()}
      </div>

      <nav>
        <ul className={styles.navItems}>
          <li className={styles.navItem}>
            <Link href="/">
              <a>Home</a>
            </Link>
          </li>
          <li className={styles.navItem}>
            <Link href="/todo">
              <a>Todos</a>
            </Link>
          </li>
          <li className={styles.navItem}>
            <Link href="/random">
              <a>Random</a>
            </Link>
          </li>
          <li className={styles.navItem}>
            <Link href="/swr">
              <a>Todos SWR</a>
            </Link>
          </li>

        </ul>
      </nav>
    </header>
  );
}
